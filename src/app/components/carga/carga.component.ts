import { Component, OnInit } from '@angular/core';
import { FileItem } from '../../models/file-item';
import { CargaImagenesService } from 'src/app/services/carga-imagenes.service';

@Component({
  selector: 'app-carga',
  templateUrl: './carga.component.html',
  styles: []
})
export class CargaComponent implements OnInit {
  subiendo = false;
  estaSobreElemento: boolean = false;
  archivosCarga: FileItem[] = [];

  constructor(private _cargaImagenes: CargaImagenesService) { }

  ngOnInit() {
  }

  cargarImagenes() {
    this._cargaImagenes.cargarImagenesFirebase(this.archivosCarga);
  }

  limpiarArchivos() {
    this.archivosCarga = [];
  }

  sobreElemento(event) {
    this.estaSobreElemento = event;
  }
}
