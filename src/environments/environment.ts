// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyAKb7EZbVvqp0Yq6Sh92W21_Fg_offR-oQ',
    authDomain: 'fir-fotos-b08bd.firebaseapp.com',
    databaseURL: 'https://fir-fotos-b08bd.firebaseio.com',
    projectId: 'fir-fotos-b08bd',
    storageBucket: 'fir-fotos-b08bd.appspot.com',
    messagingSenderId: '809135195449',
    appId: '1:809135195449:web:d798409197125fda1bd4e8'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
